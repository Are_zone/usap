from stemming import *
import pickle


corpus_words = {}
class_words = {}
training_data = []

  
fileIndonesia = open("indonesia.txt",'r')
fileJawa = open("jawa.txt",'r')

for baris in fileIndonesia:
    training_data.append({"class":"indonesia", "sentence":baris})

for baris in fileJawa:
    training_data.append({"class":"jawa", "sentence":baris})

classes = list(set([a['class'] for a in training_data]))

for c in classes:
    class_words[c] = []

for data in training_data:
    for kata in data['sentence'].split(' '):
        if kata not in corpus_words:
            corpus_words[kata] = 1
        else:
            corpus_words[kata] += 1

        class_words[data['class']].extend([kata])
		
with open('class.pickle', 'wb') as output:
    pickle.dump(class_words,output,protocol=pickle.HIGHEST_PROTOCOL)

with open('corpus.pickle', 'wb') as output:
    pickle.dump(corpus_words,output,protocol=pickle.HIGHEST_PROTOCOL)

print("done")
	
def calculate_class_score_commonality(sentence):
	with open('corpus.pickle', 'rb') as data:
		corpus_words_load = pickle.load(data)
    
	with open('class.pickle', 'rb') as data:
		class_words_load = pickle.load(data)
	
	score = 0
	for word in sentence.split(' '):
		if stemming(word) in class_words_load[class_name]:
			score += (1 / corpus_words_load[stemming(word)])
	return score


def clasify(kalimat):
    high_class = None
    high_score = 0

    for c in class_words.keys():
        score = calculate_class_score_commonality(sentence,c)

        if score > high_score:
            high_class = c
            high_score = score

    return high_class, high_score
