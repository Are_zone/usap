# import StemmerFactory class
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()


hasilKorpus = open("jawa.txt","w")
listBaris = []
try:
    with open('korpus jawa.txt', encoding='utf-8') as f:
        for line in f:
            line = line.lower()
            line = stemmer.stem(line)
            listBaris.append(line)
    for baris in listBaris:
        kalimat = ""
        listKata = baris.split(" ")
        for kata in listKata:
            if(kata != "-"):
                kalimat = kalimat+" "+kata
        print(kalimat[1:],file=hasilKorpus)
except:
    pass

    


hasilKorpus.close()
