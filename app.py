# mybot/app.py

import os
import pickle
from decouple import config
from training import *
from flask import (
    Flask, request, abort
)
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import InvalidSignatureError
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)
app = Flask(__name__)
# get LINE_CHANNEL_ACCESS_TOKEN from your environment variable
line_bot_api = LineBotApi(
    config("LINE_CHANNEL_ACCESS_TOKEN",
           default=os.environ.get('LINE_ACCESS_TOKEN'))
)
# get LINE_CHANNEL_SECRET from your environment variable
handler = WebhookHandler(
    config("LINE_CHANNEL_SECRET",
           default=os.environ.get('LINE_CHANNEL_SECRET'))
)

@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']


    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)


    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)


    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):
    line_bot_api.reply_message(
        reply = ""
		if (event.message.text == "/info"):
			reply = "Untuk mengidentifikasi bahasa daerah apa yang sedang kamu gunakan gunakan keyword /bahasa diikuti dengan kalimat yang kamu ketikkan \n Bot ini dapat mengenali bahasa jawa dan bahasa Indonesia. Untuk bahasa daerah lainnya masih belum bisa dikenali." 
		else if (event.message.text[0] == "/bahasa"):
			kelas = classify(event.message.text)[0]
			value = classify(event.message.text)[1]
			if (kelas == None):
				reply = "kami tidak bisa mengindentifikasi bahasa yang kamu gunakan"
			else:
				reply = "bahasa yang kamu gunakan adalah bahasa" + kelas + "dengan nilai akurasi " + value
		else:
			reply = "input yang kamu masukkan tidak benar. Gunakan keyword berikut: \n 1./info \n 2. /bahasa [kata yang kamu ketikkan]"
		event.reply_token,
        TextSendMessage(text=reply)
    )



if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
